﻿using MediatR;
using MovieWatcher.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace MovieWatcher.Service.CQRS.Commands.RegisterUserCommand
{
    /// <summary>
    /// Command to create user 
    /// </summary>
    public class CreateUserCommand : IRequest<ActionStatus>
    {
        /// <summary>
        /// User login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        public string LastName { get; set; }
    }
}
