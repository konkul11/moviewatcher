﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB.Models;

namespace MovieWatcher.Service.CQRS.Commands.UserCommands
{
    /// <summary>
    /// User command to update
    /// </summary>
    public class UpdateUserCommand : MWUser, IRequest<ActionStatus>
    {
    }
}
