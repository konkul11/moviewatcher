﻿using MediatR;
using MovieWatcher.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace MovieWatcher.Service.CQRS.Commands.UserCommands
{
    /// <summary>
    /// Command to delete user
    /// </summary>
    public class DeleteUserCommand : IRequest<ActionStatus>
    {
        /// <summary>
        /// User id
        /// </summary>
        public long Id { get; set; }
    }
}
