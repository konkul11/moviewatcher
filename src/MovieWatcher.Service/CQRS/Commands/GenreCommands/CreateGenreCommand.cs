﻿using MediatR;
using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.CQRS.Commands.GenreCommands
{
    /// <summary>
    /// The CreateGenreCommand class implementation.
    /// The class is used to create new Genre Command.
    /// </summary>
    public class CreateGenreCommand : IRequest<ActionStatus>
    {
        /// <summary>
        /// The Genre name
        /// </summary>
        public string Name { get; private set; }
    }
}
