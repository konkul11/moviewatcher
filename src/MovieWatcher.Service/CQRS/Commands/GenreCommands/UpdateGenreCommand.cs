﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.CQRS.Commands.GenreCommands
{
    /// <summary>
    /// The UpdateGenreCommand class implementation.
    /// The class is used to update Genre model.
    /// </summary>
    public class UpdateGenreCommand : MWGenre, IRequest<ActionStatus>
    {
    }
}
