﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.CQRS.Commands.GenreCommands
{
    /// <summary>
    /// The DeleteGenreCommand class implementation.
    /// The class is used to delete Genre model in database.
    /// </summary>
    public class DeleteGenreCommand : MWGenre, IRequest<ActionStatus>
    {
    }
}
