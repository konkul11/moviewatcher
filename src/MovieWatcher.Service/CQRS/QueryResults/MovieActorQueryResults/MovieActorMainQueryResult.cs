namespace MovieWatcher.Service.CQRS.QueryResults.MovieActorQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class MovieActorMainQueryResult
    {
        /// <summary>
        /// Movie actor id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Actor id
        /// </summary>
        public long MWActorId { get; set; }

        /// <summary>
        /// Character name in movie
        /// </summary>
        public string CharacterName { get; set; }
    }
}