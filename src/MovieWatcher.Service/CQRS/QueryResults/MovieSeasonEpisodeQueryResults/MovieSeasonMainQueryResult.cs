namespace MovieWatcher.Service.CQRS.QueryResults.MovieSeasonEpisodeQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class MovieSeasonMainQueryResult
    {
        /// <summary>
        /// Movie season id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Episode number
        /// </summary>
        public short EpisodeNumber { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }
    }
}