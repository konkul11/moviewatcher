﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult
{
    /// <summary>
    /// The GenreQueryResult class implementation.
    /// The class represents the result of the query to database - table genre.
    /// </summary>
    public class GetGenreQueryResult
    {
        /// <summary>
        /// The genre id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Genre name
        /// </summary>
        public string Name { get; set; }
    }
}
