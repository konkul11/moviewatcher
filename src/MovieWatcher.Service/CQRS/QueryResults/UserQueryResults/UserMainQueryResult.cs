﻿using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.CQRS.Queries.UserQueries
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class UserMainQueryResult
    {
        /// <summary>
        /// This id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// User status. Activ or not
        /// </summary>
        public UserStatus Status { get; set; }

        /// <summary>
        /// User role
        /// </summary>
        public UserRole Role { get; set; }
    }
}
