namespace MovieWatcher.Service.CQRS.QueryResults.FavouriteQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class FavouriteMainQueryResult
    {
        /// <summary>
        /// Favourite id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// User id
        /// </summary>
        public long MWUserId { get; set; }

    }
}