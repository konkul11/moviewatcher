using System;

namespace MovieWatcher.Service.CQRS.QueryResults.MovieRateQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class MovieRateMainQueryResult
    {
        /// <summary>
        /// Movie rate id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// User id 
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Insert date to movie rate table
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public byte Rate { get; set; }
    }
}