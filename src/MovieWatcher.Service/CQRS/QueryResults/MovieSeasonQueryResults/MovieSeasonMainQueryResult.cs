namespace MovieWatcher.Service.CQRS.QueryResults.MovieSeasonQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class MovieSeasonMainQueryResult
    {
        /// <summary>
        /// Movie season id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Season number
        /// </summary>
        public byte SeasonNumber { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }
    }
}