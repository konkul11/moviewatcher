namespace MovieWatcher.Service.CQRS.QueryResults.SoundTypeQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class SoundTypeMainQueryResult
    {
        /// <summary>
        /// Sound id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Sound name
        /// </summary>
        public string Name { get; set; }
    }
}