using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class CommentMainQueryResult
    {
        /// <summary>
        /// Comment id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// User id who add commnet
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Comment text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Update date
        /// </summary>
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }
    }
}
