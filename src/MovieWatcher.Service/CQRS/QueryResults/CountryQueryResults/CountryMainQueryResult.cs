namespace MovieWatcher.Service.CQRS.QueryResults.CountryMainQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class CountryMainQueryReuslt
    {
        /// <summary>
        /// Country id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Country name
        /// </summary>
        public string Name { get; set; }
    }
}