namespace MovieWatcher.Service.CQRS.QueryResults.MovieCountryQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class MovieCountryMainQueryResult
    {
        /// <summary>
        /// Movie country id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Country id
        /// </summary>
        public long MWCountryId { get; set; }
    }
}