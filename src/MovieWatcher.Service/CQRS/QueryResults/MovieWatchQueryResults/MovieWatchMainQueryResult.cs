using System;

namespace MovieWatcher.Service.CQRS.QueryResults.MovieWatchQueryResults
{
    /// <summary>
    /// Main class to return most popular fields
    /// </summary>
    public class MovieWatchMainQueryResult
    {
        /// <summary>
        /// Movie watch id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Movie id
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// User id
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Inser date to movie watch table
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Watch time movie
        /// </summary>
        public TimeSpan WatchingTime { get; set; }

    }
}