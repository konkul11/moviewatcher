using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Queries.CommentQueries;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.CommentQueryHandler
{
    /// <summary>
    /// Query handler class to get all comments
    /// </summary>
    public class GetAllCommentsQueryHandler : IRequestHandler<GetAllCommentQuery, List<CommentMainQueryResult>>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public GetAllCommentsQueryHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Query handler method to get all comments
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<CommentMainQueryResult>> Handle(GetAllCommentQuery command, CancellationToken cancellationToken)
        {
            List<CommentMainQueryResult> result = null;
            try
            {
                var comments = await _db.Comments.Where(x => !x.IsDeleted).ToListAsync();
                result = Mapper.Map<List<CommentMainQueryResult>>(comments);
                this._logger.LogMessage("The comments has been downloaded.", LogLevel.Info);
            }
            catch (Exception ex)
            {
                result = null;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}