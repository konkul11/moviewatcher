using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Queries.CommentQueries;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.CommentQueryHandler
{
    /// <summary>
    /// Query handler class to get comment by id
    /// </summary>
    public class GetCommentByIdQueryHandler : IRequestHandler<GetCommentByIdQuery, CommentMainQueryResult>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctor
        /// <summary>
        /// The ctor 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public GetCommentByIdQueryHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");
            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Handle methods to get commnet by id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CommentMainQueryResult> Handle(GetCommentByIdQuery query, CancellationToken cancellationToken)
        {
            CommentMainQueryResult result = null;
            try
            {
                var comment = await _db.Comments.Where(x => x.Id == query.Id && !x.IsDeleted).SingleOrDefaultAsync();
                if (comment != null)
                {
                    result = Mapper.Map<CommentMainQueryResult>(comment);
                    this._logger.LogMessage("The comment has been downloaded.", LogLevel.Info);
                }
                else
                {
                    this._logger.LogMessage("The comment on this Id not exists.", LogLevel.Info);
                    this._logger.LogMessage(new LogMessage
                    {
                        Data = query,
                        ClassName = this.ToString(),
                        Message = "The comment on this Id not exists.",
                        MethodName = "Handle",
                        LogLevel = MessageType.Debug
                    }, LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}