using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Service.CQRS.Queries.CommentQueries;
using MovieWatcher.Service.CQRS.Queries.UserQueries;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.CommentQueryHandler
{
    /// <summary>
    /// Handler class to get comment by user id
    /// </summary>
    public class GetCommentByUserIdHandler : IRequestHandler<GetCommentByUserIdQuery, CommentMainQueryResult>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctor
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public GetCommentByUserIdHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");
            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Handler method to get comment by user id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CommentMainQueryResult> Handle(GetCommentByUserIdQuery query, CancellationToken cancellationToken)
        {
            CommentMainQueryResult result = null;
            try
            {
                var comment = await _db.Comments.Where(x => x.Id == query.UserId && !x.IsDeleted).SingleOrDefaultAsync();
                if (comment != null)
                {
                    result = Mapper.Map<CommentMainQueryResult>(comment);
                    this._logger.LogMessage("The comment has been downloaded.", LogLevel.Info);
                }
                else
                {
                    this._logger.LogMessage("The comment on this userId not exists.", LogLevel.Info);
                    this._logger.LogMessage(new LogMessage
                    {
                        Data = query,
                        ClassName = this.ToString(),
                        Message = "The comment on this userId not exists.",
                        MethodName = "Handle",
                        LogLevel = MessageType.Debug
                    }, LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}