using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Service.CQRS.Queries.GenreQueries;
using MovieWatcher.Service.CQRS.Queries.UserQueries;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.UserQueryHandlers
{
    /// <summary>
    /// Query handler class to get user by id
    /// </summary>
    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, UserMainQueryResult>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region 
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public GetUserByIdQueryHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");
            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Handle method to get user by id
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UserMainQueryResult> Handle(GetUserByIdQuery query, CancellationToken cancellationToken)
        {
            UserMainQueryResult result = null;
            try
            {
                var user = await _db.Users.Where(x => x.Id == query.Id && !x.IsDeleted).SingleOrDefaultAsync();
                if (user != null)
                {
                    result = Mapper.Map<UserMainQueryResult>(user);
                    this._logger.LogMessage("The user has been downloaded.", LogLevel.Info);
                }
                else
                {
                    this._logger.LogMessage("The user on this Id not exists.", LogLevel.Info);
                    this._logger.LogMessage(new LogMessage
                    {
                        Data = query,
                        ClassName = this.ToString(),
                        Message = "The user on this Id not exists.",
                        MethodName = "Handle",
                        LogLevel = MessageType.Debug
                    }, LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}