﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Service.CQRS.Queries.UserQueries;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.UserQueryHandlers
{
    public class GetAllUsersQueryHandler : IRequestHandler<GetAllUsersQuery, List<UserMainQueryResult>>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public GetAllUsersQueryHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Handle method to get all users
        /// </summary>
        /// <param name="getAllUsersQuery"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<UserMainQueryResult>> Handle(GetAllUsersQuery getAllUsersQuery, CancellationToken cancellationToken)
        {
            var result = new List<UserMainQueryResult>();
            try
            {
                var users = await _db.Users.Where(x => !x.IsDeleted).ToListAsync();
                result = Mapper.Map<List<UserMainQueryResult>>(users);
                this._logger.LogMessage("The users has been downloaded.", LogLevel.Info);
            }
            catch (Exception ex)
            {
                result = null;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}
