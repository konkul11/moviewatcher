﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Service.CQRS.Queries.GenreQueries;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.GenreQueryHandlers
{
    /// <summary>
    /// The GetAllGenresQueryHandler class implementation.
    /// </summary>
    public class GetAllGenresQueryHandler : IRequestHandler<GetAllGenreQuery, List<GetGenreQueryResult>>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAllGenresQueryHandler" /> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        public GetAllGenresQueryHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion


        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query">The query object.</param>
        /// <param name="cancellationToken">The cancellation Token object.</param>
        /// <returns>The list of GetGenreQueryResult objects.</returns>
        public async Task<List<GetGenreQueryResult>> Handle(GetAllGenreQuery query, CancellationToken cancellationToken)
        {
            var result = new List<GetGenreQueryResult>();
            try
            {
                var genres = _db.Genres.Where(x => !x.IsDeleted);
                if (genres != null)
                {
                    foreach (var genre in genres)
                    {
                        result.Add(new GetGenreQueryResult
                        {
                            Name = genre.Name,
                            Id = genre.Id
                        });
                    }
                    this._logger.LogMessage("The genres has been downloaded.", LogLevel.Info);
                }
                else
                {
                    result = null;
                    this._logger.LogMessage("The genre tables is empty or all data is deleted.",LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                result = null;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return await Task.FromResult(result);
        }

        #endregion
    }
}
