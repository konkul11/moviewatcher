﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Service.CQRS.Queries.GenreQueries;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.QueryHandlers.GenreQueryHandlers
{
    /// <summary>
    /// The GetGenreByNameQueryHandler class implementation.
    /// </summary>
    public class GetGenreByNameQueryHandler : IRequestHandler<GetGenreByNameQuery, GetGenreQueryResult>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="GetGenreByNameQueryHandler" /> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        public GetGenreByNameQueryHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods

        public async Task<GetGenreQueryResult> Handle(GetGenreByNameQuery query, CancellationToken cancellationToken)
        {
            GetGenreQueryResult result = null;
            try
            {
                var genre = _db.Genres.Where(x => !x.IsDeleted && x.Name == query.Name).SingleOrDefault();
                if (genre != null)
                {
                    result = new GetGenreQueryResult()
                    {
                        Name = genre.Name,
                        Id = genre.Id
                    };
                    this._logger.LogMessage("The genre has beend downloaded.", LogLevel.Info);
                }
                else
                {
                    result = null;
                    this._logger.LogMessage("The genre on this name not exists.", LogLevel.Info);
                    this._logger.LogMessage(new LogMessage
                    {
                        Data = query,
                        ClassName = this.ToString(),
                        Message = "The genre on this name not exists.",
                        MethodName = "Handle",
                        LogLevel = MessageType.Debug
                    }, LogLevel.Debug);
                }
            }
            catch(Exception ex)
            {
                result = null;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}
