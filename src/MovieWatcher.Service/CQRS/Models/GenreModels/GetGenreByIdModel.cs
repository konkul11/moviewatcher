﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Service.CQRS.Models.GenreModels
{
    /// <summary>
    /// The GetGenreByNameModel class implementaiton.
    /// This model is used to get data from db about the genre by id.
    /// </summary>
    public class GetGenreByIdModel
    {
        /// <summary>
        /// The genre id.
        /// </summary>
        [Required]
        public long Id { get; set; }
    }
}
