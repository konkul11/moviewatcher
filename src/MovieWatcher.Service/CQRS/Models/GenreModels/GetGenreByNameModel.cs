﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Service.CQRS.Models.GenreModels
{
    /// <summary>
    /// The GetGenreByNameModel class implementaiton.
    /// This model is used to get data from db about the genre by name.
    /// </summary>
    public class GetGenreByNameModel
    {
        /// <summary>
        /// The genre name.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
