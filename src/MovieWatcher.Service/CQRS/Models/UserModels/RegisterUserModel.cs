﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Service.CQRS.Models.UserModels
{
    /// <summary>
    /// The class represents user
    /// </summary>
    public class RegisterUserModel
    {
        /// <summary>
        /// User login
        /// </summary>
        [Required]
        [StringLength(25, MinimumLength = 6)]
        public string Login { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required]
        [StringLength(25, MinimumLength = 6)]
        public string Password { get; set; }

        /// <summary>
        /// User first name 
        /// </summary>
        [Required]
        [StringLength(25, MinimumLength = 3)]
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string LastName { get; set; }
    }
}
