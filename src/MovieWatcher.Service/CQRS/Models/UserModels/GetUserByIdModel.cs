using System.ComponentModel.DataAnnotations;

namespace MovieWatcher.Service.CQRS.Models.UserModels
{
    /// <summary>
    /// Model to get user by id
    /// </summary>
    public class GetUserByIdModel
    {
        /// <summary>
        /// User id
        /// </summary>
        [Required]
        public long Id { get; set; }
    }
}