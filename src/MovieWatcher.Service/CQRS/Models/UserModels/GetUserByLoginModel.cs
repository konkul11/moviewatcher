﻿using System.ComponentModel.DataAnnotations;

namespace MovieWatcher.Service.CQRS.Models.UserModels
{
    /// <summary>
    /// Model to get user by login
    /// </summary>
    public class GetUserByLoginModel
    {
        /// <summary>
        /// User login
        /// </summary>
        [Required]
        [StringLength(25, MinimumLength = 6)]
        public string Login { get; set; }
    }
}
