using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Common.Helpers;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.UserCommands;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.CommandHandlers.UserCommandHandlers
{
    /// <summary>
    /// Class to update user
    /// </summary>
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, ActionStatus>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateUserCommandHandler" /> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        /// <param name="logger">The logger service.</param>
        public UpdateUserCommandHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Handle to update user
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ActionStatus> Handle(UpdateUserCommand command, CancellationToken cancellationToken)
        {
            var result = ActionStatus.NotModified;
            try
            {
                var user = await this._db.Users.Where(x => x.Id == command.Id && !x.IsDeleted).SingleOrDefaultAsync();
                if (user != null)
                {
                    try
                    {
                        MWUser userNew = Mapper.Map<MWUser>(command);
                        userNew.RegistrationDate = DateTime.Now;
                        userNew.Salt = Guid.NewGuid();
                        userNew.Password = PasswordHelper.SHA512(command.Password);
                        userNew.Role = UserRole.User;
                        _db.Users.Add(userNew);
                        _db.SaveChanges();
                        this._db.Entry(user).CurrentValues.SetValues(user);
                        this._db.SaveChanges();
                        result = ActionStatus.Updated;
                        this._logger.LogMessage("The user has been updated.", LogLevel.Info);
                    }
                    catch (Exception ex)
                    {
                        result = ActionStatus.NotModified;
                        this._logger.LogMessage("The error while updating user.", LogLevel.Debug);
                        this._logger.LogMessage(new LogMessage
                        {
                            Message = "The error while updating user.",
                            Exception = ex,
                            ClassName = this.ToString(),
                            MethodName = "Handle",
                            LogLevel = MessageType.Error,
                            Data = command
                        }, LogLevel.Error);
                    }
                }
                else
                {
                    result = ActionStatus.NotFound;
                    this._logger.LogMessage("The user on this name not exist.", LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                result = ActionStatus.NotModified;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}