﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Service.CQRS.Commands.UserCommands;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.CommandHandlers.UserCommandHandlers
{
    /// <summary>
    /// This class is for ....
    /// </summary>
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, ActionStatus>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db = new MWContext();

        /// <summary>
        /// The mediator
        /// </summary>
        private readonly IMediator _mediator;

        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion


        #region Ctors
        /// <summary>
        /// The command handler to remove user
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="mediator"></param>
        /// <param name="logger"></param>
        public DeleteUserCommandHandler(MWContext dbContext, IMediator mediator, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");
            this._db = dbContext;
            this._mediator = mediator;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ActionStatus> Handle(DeleteUserCommand command, CancellationToken cancellationToken)
        {
            var result = ActionStatus.NotDeleted;
            try
            {
                var user = await this._db.Users.Where(x => x.Id == command.Id).FirstOrDefaultAsync();
                if (user != null)
                {
                    var usingUserList = new List<DeleteUserCommand>();
                    if (usingUserList == null && !usingUserList.Any())
                    {
                        this._db.Remove(user);
                        this._db.SaveChanges();
                        result = ActionStatus.Deleted;
                    }
                    else
                        result = ActionStatus.DataIsUsing;
                }
                else
                {
                    result = ActionStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                result = ActionStatus.NotDeleted;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}
