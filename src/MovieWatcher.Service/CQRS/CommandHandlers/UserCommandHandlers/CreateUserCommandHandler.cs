﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MovieWatcher.Common.Enums;
using MovieWatcher.Common.Helpers;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.RegisterUserCommand;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS
{
    /// <summary>
    /// User command handler - create 
    /// </summary>
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, ActionStatus>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctor
        /// <summary>
        /// Ctor class with injecton two parameters 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="logger"></param>
        public CreateUserCommandHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Method to create user
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ActionStatus> Handle(CreateUserCommand command, CancellationToken cancellationToken)
        {
            var result = ActionStatus.NotCreated;
            try
            {
                var user = await this._db.Users.Where(x => x.Login == command.Login && !x.IsDeleted).FirstOrDefaultAsync();
                if (user == null)
                {
                    try
                    {
                        MWUser userNew = Mapper.Map<MWUser>(command);
                        userNew.RegistrationDate = DateTime.Now;
                        userNew.Salt = Guid.NewGuid();
                        userNew.Password = PasswordHelper.SHA512(command.Password);
                        userNew.Role = UserRole.User;
                        user.Status = UserStatus.Active;
                        _db.Users.Add(userNew);
                        _db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        result = ActionStatus.NotCreated;
                        this._logger.LogMessage("The error while creating user.", LogLevel.Debug);
                        this._logger.LogMessage(new LogMessage
                        {
                            Message = "The error while creating genere.",
                            Exception = ex,
                            ClassName = this.ToString(),
                            MethodName = "Handle",
                            LogLevel = MessageType.Error,
                            Data = command
                        }, LogLevel.Error);
                    }
                }
                else
                {
                    result = ActionStatus.DataAlreadyExists;
                    this._logger.LogMessage("The user on this name already exist.", LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                result = ActionStatus.NotCreated;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
            return result;
        }
        #endregion
    }
}
