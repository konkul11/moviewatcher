﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.GenreCommands;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.CommandHandlers.GenreCommandHandlers
{
    /// <summary>
    /// The CreateGenreCommandHandler class implementation.
    /// </summary>
    public class CreateGenreCommandHandler : IRequestHandler<CreateGenreCommand, ActionStatus>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateGenreCommandHandler" /> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        /// <param name="logger">The logger service.</param>
        public CreateGenreCommandHandler(MWContext dbContext,ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");
           
            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion


        /// <summary>
        /// The handle method, used to creating new instace of Genre.
        /// </summary>
        /// <param name="request">The genre object to create.</param>
        /// <param name="cancellationToken">The cancellation token. <seealso cref="System.Threading.CancellationToken"/> </param>
        /// <returns>The action status.</returns>
        public Task<ActionStatus> Handle(CreateGenreCommand request, CancellationToken cancellationToken)
        {
            var result = ActionStatus.NotCreated;
            try
            {
                var Genre = this._db.Genres.Where(genre => genre.Name.Contains(request.Name)).FirstOrDefault();
                if (Genre == null)
                {
                    try
                    {
                        //User automapper here
                        var nGenre = new MWGenre() { Name = request.Name, IsDeleted = false };
                        this._db.Add(nGenre);
                        this._db.SaveChanges();
                        result = ActionStatus.Created;
                        //Info log
                        //Data has been created
                        this._logger.LogMessage("The genre has been created.", LogLevel.Info);
                    }
                    catch (Exception ex)
                    {
                        result = ActionStatus.NotCreated;
                        this._logger.LogMessage("The error while creating genere.", LogLevel.Debug);
                        this._logger.LogMessage(new LogMessage
                        {
                            Message = "The error while creating genere.",
                            Exception = ex,
                            ClassName = this.ToString(),
                            MethodName = "Handle",
                            LogLevel = MessageType.Error,
                            Data = request
                        }, LogLevel.Error);
                    }
                }
                else
                {
                    result = ActionStatus.DataAlreadyExists;
                    this._logger.LogMessage("The gener on this name already exist.", LogLevel.Debug);
                }
            }
            catch(Exception ex)
            {
                result = result = ActionStatus.NotCreated;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }
                
            return Task.FromResult(result);
        }
    }
}
