﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.GenreCommands;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.CommandHandlers.GenreCommandHandlers
{
    /// <summary>
    /// The UpdateGenreCommandHandler class implementation.
    /// </summary>
    public class UpdateGenreCommandHandler : IRequestHandler<UpdateGenreCommand, ActionStatus>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _db;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateGenreCommandHandler" /> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        /// <param name="logger">The logger service.</param>
        public UpdateGenreCommandHandler(MWContext dbContext, ILoggerService logger)
        {
            if (dbContext == null)
                throw new ArgumentNullException("The dbcontext cannot be null.");

            if (logger == null)
                throw new ArgumentNullException("The logger service cannot be null.");

            this._db = dbContext;
            this._logger = logger;
        }
        #endregion


        /// <summary>
        /// The handle method, used to updating instace of Genre.
        /// </summary>
        /// <param name="request">The genre object to update.</param>
        /// <param name="cancellationToken">The cancellation token. <seealso cref="System.Threading.CancellationToken"/> </param>
        /// <returns>The action status.</returns>
        public Task<ActionStatus> Handle(UpdateGenreCommand request, CancellationToken cancellationToken)
        {
            var result = ActionStatus.NotModified;
            try
            {
                var Genre = this._db.Genres.Where(genre => genre.Id == request.Id).FirstOrDefault();
                if (Genre != null)
                {
                    try
                    {
                        //User automapper here
                        var uGenre = new MWGenre()
                        {
                            Id = request.Id,
                            Name = request.Name,
                            IsDeleted = request.IsDeleted
                        };
                        this._db.Entry(Genre).CurrentValues.SetValues(uGenre);
                        this._db.SaveChanges();
                        result = ActionStatus.Updated;
                        this._logger.LogMessage("The genre has been updated.", LogLevel.Info);
                    }
                    catch (Exception ex)
                    {
                        result = ActionStatus.NotModified;
                        this._logger.LogMessage("The error while updating genere.", LogLevel.Debug);
                        this._logger.LogMessage(new LogMessage
                        {
                            Message = "The error while updating genere.",
                            Exception = ex,
                            ClassName = this.ToString(),
                            MethodName = "Handle",
                            LogLevel = MessageType.Error,
                            Data = request
                        }, LogLevel.Error);
                    }
                }
                else
                {
                    result = ActionStatus.NotFound;
                    this._logger.LogMessage("The gener on this name not exist.", LogLevel.Debug);
                }
            }
            catch (Exception ex)
            {
                result = ActionStatus.NotModified;
                this._logger.LogMessage(new LogMessage
                {
                    Message = "The database connection error.",
                    Exception = ex,
                    ClassName = this.ToString(),
                    MethodName = "Handle",
                    LogLevel = MessageType.Error
                }, LogLevel.Error);
            }

            return Task.FromResult(result);
        }
    }
}
