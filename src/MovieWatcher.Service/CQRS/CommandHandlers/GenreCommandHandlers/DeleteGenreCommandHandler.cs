﻿using MediatR;
using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.GenreCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieWatcher.Service.CQRS.CommandHandlers.GenreCommandHandlers
{
    /// <summary>
    /// The DeleteGenreCommandHandler class implementation.
    /// </summary>
    public class DeleteGenreCommandHandler : IRequestHandler<DeleteGenreCommand, ActionStatus>
    {
        #region Fields
        /// <summary>
        /// The movie watche database context.
        /// </summary>
        private readonly MWContext _Db = new MWContext();

        /// <summary>
        /// The mediator
        /// </summary>
        private readonly IMediator _Mediator;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateGenreCommandHandler" /> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        /// <param name="mediator">The mediator.</param>
        public DeleteGenreCommandHandler(MWContext dbContext, IMediator mediator)
        {
            this. _Db = dbContext;

            //This mediator cannot be here!!
            //ToDo: 
            // Mediator PipeLine
            this._Mediator = mediator;
        }
        #endregion


        /// <summary>
        /// The handle method, used to deleting instace of Genre.
        /// </summary>
        /// <param name="request">The genre object to delete.</param>
        /// <param name="cancellationToken">The cancellation token. <seealso cref="System.Threading.CancellationToken"/> </param>
        /// <returns>The action status.</returns>
        public Task<ActionStatus> Handle(DeleteGenreCommand request, CancellationToken cancellationToken)
        {
            /**
             * When Konrad Kulikowski will understand how the work MediatR - Pipeline, 
             * this should be reimplemented. 
             */
            var result = ActionStatus.NotDeleted;
            try
            {
                var Genre = this._Db.Genres.Where(genre => genre.Id == request.Id).FirstOrDefault();
                if (Genre != null)
                {
                    //Remove the following, we must use pipeline for this operation!!!
                    //var usingGenreList = this._Mediator.Send(new GenreMovieGetAll( id = 11 ));
                    var usingGenreList = new List<DeleteGenreCommand>(); // as above...
                    if (usingGenreList == null && !usingGenreList.Any())
                    {
                        this._Db.Remove(Genre);
                        this._Db.SaveChanges();
                        result = ActionStatus.Deleted;
                    }
                    else
                        result = ActionStatus.DataIsUsing;
                }
                else
                    result = ActionStatus.NotFound;
            }
            catch(Exception ex)
            {
                result = ActionStatus.NotDeleted;
                //User logger here
            }

               
            return Task.FromResult(result);
        }
    }
}
