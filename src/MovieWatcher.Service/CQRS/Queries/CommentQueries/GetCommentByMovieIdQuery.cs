using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;

namespace MovieWatcher.Service.CQRS.Queries.CommentQueries
{
    /// <summary>
    /// Query to gt comment by movie id
    /// </summary>
    public class GetCommentByMovieIdQuery : IRequest<CommentMainQueryResult>
    {
        /// <summary>
        /// Movie id
        /// </summary>
        public long MovieId { get; set; }
    }
}