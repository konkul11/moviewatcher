using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;
using System.Collections.Generic;

namespace MovieWatcher.Service.CQRS.Queries.CommentQueries
{
    /// <summary>
    /// Query to get all comments
    /// </summary>
    public class GetAllCommentQuery : IRequest<List<CommentMainQueryResult>>
    {

    }
}