using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;

namespace MovieWatcher.Service.CQRS.Queries.CommentQueries
{
    /// <summary>
    /// Query to get  comment by id
    /// </summary>
    public class GetCommentByIdQuery : IRequest<CommentMainQueryResult>
    {
        /// <summary>
        /// Commment id
        /// </summary>
        public long? Id { get; set; }
    }
}