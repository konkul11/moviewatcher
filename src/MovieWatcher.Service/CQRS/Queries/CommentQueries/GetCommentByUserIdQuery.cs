using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.CommentQueryResult;
using System.ComponentModel.DataAnnotations;

namespace MovieWatcher.Service.CQRS.Queries.CommentQueries
{
    /// <summary>
    /// Query to get comment by user id
    /// </summary>
    public class GetCommentByUserIdQuery : IRequest<CommentMainQueryResult>
    {
        /// <summary>
        /// User id
        /// </summary>
        public long UserId { get; set; }
    }
}