using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.MovieSeasonEpisodeQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.MovieSeasonQueries
{
    /// <summary>
    /// Query to get season by id
    /// </summary>
    public class GetMovieSeasonByIdQuery : IRequest<MovieSeasonMainQueryResult>
    {
        /// <summary>
        /// Season id
        /// </summary>
        public long? Id { get; set; }
    }
}