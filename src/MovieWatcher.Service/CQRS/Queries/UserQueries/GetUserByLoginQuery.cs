﻿using MediatR;

namespace MovieWatcher.Service.CQRS.Queries.UserQueries
{
    /// <summary>
    /// Query to get user by login
    /// </summary>
    public class GetUserByLoginQuery : IRequest<UserMainQueryResult>
    {
        /// <summary>
        /// User login
        /// </summary>
        public string Login { get; set; }
    }
}
