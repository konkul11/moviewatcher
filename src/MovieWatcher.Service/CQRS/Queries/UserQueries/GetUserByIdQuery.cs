using MediatR;

namespace MovieWatcher.Service.CQRS.Queries.UserQueries
{
    /// <summary>
    /// Query to get user by id
    /// </summary>
    public class GetUserByIdQuery : IRequest<UserMainQueryResult>
    {
        /// <summary>
        /// User id
        /// </summary>
        public long? Id { get; set; }
    }
}