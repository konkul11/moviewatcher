﻿using MediatR;
using System.Collections.Generic;

namespace MovieWatcher.Service.CQRS.Queries.UserQueries
{
    /// <summary>
    /// Query to get all users
    /// </summary>
    public class GetAllUsersQuery : IRequest<List<UserMainQueryResult>>
    {
    }
}
