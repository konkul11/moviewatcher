using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.MovieCountryQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.MovieCountryQueries
{
    /// <summary>
    /// Query to get movie country by id
    /// </summary>
    public class GetMovieCountryByIdQuery : IRequest<MovieCountryMainQueryResult>
    {
        /// <summary>
        /// Movie country id
        /// </summary>
        public long? Id { get; set; }
    }
}