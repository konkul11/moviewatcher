﻿using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Service.CQRS.Queries.GenreQueries
{
    /// <summary>
    /// The GetAllGenreQuery class implementaiton.
    /// </summary>
    public class GetAllGenreQuery : IRequest<List<GetGenreQueryResult>>
    {
    }
}
