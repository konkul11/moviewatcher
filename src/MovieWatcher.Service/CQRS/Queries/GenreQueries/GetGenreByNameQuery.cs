﻿using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Service.CQRS.Queries.GenreQueries
{
    /// <summary>
    /// The GetGenreByNameQuery class implementaiton.
    /// </summary>
    public class GetGenreByNameQuery : IRequest<GetGenreQueryResult>
    {
        /// <summary>
        /// The genre name.
        /// </summary>
        public string Name { get; private set; }
    }
}
