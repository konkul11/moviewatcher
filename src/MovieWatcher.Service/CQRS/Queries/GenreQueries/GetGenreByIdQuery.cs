﻿using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Service.CQRS.Queries.GenreQueries
{
    /// <summary>
    /// The GetGenreByNameModel class implementaiton.
    /// </summary>
    public class GetGenreByIdQuery : IRequest<GetGenreQueryResult>
    {
        /// <summary>
        /// The genre id.
        /// </summary>
        [Required]
        public long? Id { get; set; }
    }
}
