using MediatR;

namespace MovieWatcher.Service.CQRS.Queries.MovieWatchQueries
{
    /// <summary>
    /// Query to get movie watcher by id
    /// </summary>
    public class GetMovieWatcherByIdQuery : IRequest<MovieWatcherMainQueryResult>
    {
        /// <summary>
        /// Movie watch id
        /// </summary>
        public long? Id { get; set; }
    }
}