using MediatR;

namespace MovieWatcher.Service.CQRS.Queries.MovieWatchQueries
{
    /// <summary>
    /// Query to get movie watch by user id
    /// </summary>
    public class GetMovieWatcherByUserIdQuery : IRequest<MovieWatcherMainQueryResult>
    {
        /// <summary>
        /// User id
        /// </summary>
        public long UserId { get; set; }
    }
}