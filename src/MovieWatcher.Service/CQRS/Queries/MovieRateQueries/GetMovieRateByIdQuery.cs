using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.MovieRateQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.MovieRateQueries
{
    /// <summary>
    /// Query to get movie rate by id
    /// </summary>
    public class GetMovieRateByIdQuery : IRequest<MovieRateMainQueryResult>
    {
        /// <summary>
        /// Movie rate id
        /// </summary>
        public long? Id { get; set; }
    }
}