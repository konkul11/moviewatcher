using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.MovieRateQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.MovieRateQueries
{
    /// <summary>
    /// Query to get movie rate by user ud
    /// </summary>
    public class GetMovieRateByUserIdQuery : IRequest<MovieRateMainQueryResult>
    {
        /// <summary>
        /// User id
        /// </summary>
        public long UserId { get; set; }
    }
}