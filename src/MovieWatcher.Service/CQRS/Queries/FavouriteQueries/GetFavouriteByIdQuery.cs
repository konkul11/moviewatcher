using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.FavouriteQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.FavouriteQueries
{
    /// <summary>
    /// Query to get favourite by id
    /// </summary>
    public class GetFavouriteByIdQuery : IRequest<FavouriteMainQueryResult>
    {
        /// <summary>
        /// Favourite id
        /// </summary>
        public long? Id { get; set; }
    }
}