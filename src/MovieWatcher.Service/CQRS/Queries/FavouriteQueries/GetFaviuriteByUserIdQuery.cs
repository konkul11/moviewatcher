using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.FavouriteQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.FavouriteQueries
{
    /// <summary>
    /// Query to  get favourite by user id
    /// </summary>
    public class GetFaviuriteByUserIdQuery : IRequest<FavouriteMainQueryResult>
    {
        /// <summary>
        /// User id
        /// </summary>
        public long UserId { get; set; }
    }
}