using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.SoundTypeQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.SoundTypeQueries
{
    /// <summary>
    /// Query to get sound type by id
    /// </summary>
    public class GetSoundTypeByIdQuery : IRequest<SoundTypeMainQueryResult>
    {
        /// <summary>
        /// Sound type id
        /// </summary>
        public long? Id { get; set; }
    }
}