using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.SoundTypeQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.SoundTypeQueries
{
    /// <summary>
    /// Query to get sound type by name
    /// </summary>
    public class GetSoundTypeQuery : IRequest<SoundTypeMainQueryResult>
    {
        /// <summary>
        /// Sound name
        /// </summary>
        public string Name { get; set; }
    }
}