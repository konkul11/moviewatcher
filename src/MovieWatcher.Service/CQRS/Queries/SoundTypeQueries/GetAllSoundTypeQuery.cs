using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.SoundTypeQueryResults;
using System.Collections.Generic;

namespace MovieWatcher.Service.CQRS.Queries.SoundTypeQueries
{
    /// <summary>
    /// Query to get all sounds type
    /// </summary>
    public class GetAllSoundTypeQuery : IRequest<List<SoundTypeMainQueryResult>>
    {

    }
}