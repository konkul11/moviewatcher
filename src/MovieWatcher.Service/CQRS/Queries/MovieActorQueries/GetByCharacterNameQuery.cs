using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.MovieActorQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.MovieActorQueries
{
    /// <summary>
    /// Query to get movie actor by character name 
    /// </summary>
    public class GetByCharacterNameQuery : IRequest<MovieActorMainQueryResult>
    {
        /// <summary>
        /// Character name
        /// </summary>
        public string CharacterName { get; set; }
    }
}