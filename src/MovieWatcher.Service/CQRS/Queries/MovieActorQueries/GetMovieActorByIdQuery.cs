using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.MovieActorQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.MovieActorQueries
{
    /// <summary>
    /// Query to get movie actor by id
    /// </summary>
    public class GetMovieActorByIdQuery : IRequest<MovieActorMainQueryResult>
    {
        /// <summary>
        /// Movie ator id
        /// </summary>
        public long? Id { get; set; }
    }
}