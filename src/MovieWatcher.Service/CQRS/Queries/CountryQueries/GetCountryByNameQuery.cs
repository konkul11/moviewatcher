using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.CountryMainQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.CountryQueries
{
    /// <summary>
    /// Query to get country by name
    /// </summary>
    public class GetCountryByNameQuery : IRequest<CountryMainQueryReuslt>
    {
        /// <summary>
        /// Country name
        /// </summary>
        public string Name { get; set; }
    }
}