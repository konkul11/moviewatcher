using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.CountryMainQueryResults;

namespace MovieWatcher.Service.CQRS.Queries.CountryQueries
{
    /// <summary>
    /// Get country by country id
    /// </summary>
    public class GetCountryByIdQuery : IRequest<CountryMainQueryReuslt>
    {
        /// <summary>
        /// Country id
        /// </summary>
        public long? Id { get; set; }
    }
}