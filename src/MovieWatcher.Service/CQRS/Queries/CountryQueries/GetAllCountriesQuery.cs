using MediatR;
using MovieWatcher.Service.CQRS.QueryResults.SoundTypeQueryResults;
using System.Collections.Generic;

namespace MovieWatcher.Service.CQRS.Queries.CountryQueries
{
    /// <summary>
    /// Query to get all countries 
    /// </summary>
    public class GetAllCountriesQuery : IRequest<List<SoundTypeMainQueryResult>>
    {

    }
}