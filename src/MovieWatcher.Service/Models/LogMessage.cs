﻿using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Models
{
    public class LogMessage
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LogMessage" /> class.
        /// </summary>
        public LogMessage()
        {
            this.MethodName = "";
            this.Message = "";
            this.ClassName = "";
            this.Exception = null;
            this.MethodName = "";
            this.LogLevel = MessageType.Info;
            this.Data = null;
        }

        /// <summary>
        ///     Gets or sets the application.
        /// </summary>
        /// <value>
        ///     The application.
        /// </value>
        public string Application { get; set; }
        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        /// <value>
        ///     Contents of log.
        /// </value>
        public string Message { get; set; }
        /// <summary>
        ///     Gets or sets the name of the class.
        /// </summary>
        /// <value>
        ///     The name of the class.
        /// </value>
        public string ClassName { get; set; }
        /// <summary>
        ///     Gets or sets the exception.
        /// </summary>
        /// <value>
        ///     The exception.
        /// </value>
        // TODO: Custom exception handling
        public Exception Exception { get; set; }
        /// <summary>
        ///     Gets or sets the log time.
        /// </summary>
        /// <value>
        ///     The log time.
        /// </value>
        public DateTime LogTime => DateTime.Now;
        /// <summary>
        ///     Gets or sets the name of the method.
        /// </summary>
        /// <value>
        ///     The name of the method.
        /// </value>
        public string MethodName { get; set; }
        /// <summary>
        ///     Gets or sets the log level.
        /// </summary>
        /// <value>
        ///     The log level.
        /// </value>
        public MessageType LogLevel { get; set; }

        /// <summary>
        ///     Gets or sets the data.
        /// </summary>
        /// <value>
        ///     Unspecific data at which an error occurred.
        /// </value>
        public object Data { get; set; }
    }
}
