﻿using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Models
{
    /// <summary>
    ///     MessageResponse class implementation.
    ///     It is used to store information exchanged between the web service and the external system.
    /// </summary>
    public class MessageResponse
    {
        /// <summary>
        ///     Gets or sets the response code.
        /// </summary>
        /// <value>
        ///     The status code.
        /// </value>
        public ResponseStatus ResponseCode { get; set; }
        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        /// <value>
        ///     The message which is sent to eternal system.
        ///     Usually contains invoked method result or exception string.
        /// </value>
        public string Message { get; set; }
        /// <summary>
        ///     Gets or sets the data.
        /// </summary>
        /// <value>
        ///     Unspecific data that can be sent to external system.
        /// </value>
        public Object[] Data { get; set; }
        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>
        ///  The selected page number.
        /// </value>
        public uint PageNumber { get; set; }
        /// <summary>
        /// Gets or sets total page number.
        /// </summary>
        /// <value>
        ///  The total page count.
        /// </value>
        public uint TotalPageCount { get; set; }
    }
}
