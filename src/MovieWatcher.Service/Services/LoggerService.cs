﻿using MovieWatcher.Common.Helpers;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Services
{
    /// <summary>
    ///     LoggerService implementation.
    /// </summary>
    /// <seealso cref="ILoggerService" />
    public class LoggerService : ILoggerService
    {
        #region Fields

        /// <summary>
        ///     The logger
        /// </summary>
        private readonly Logger _logger;
        /// <summary>
        ///     The application name
        /// </summary>
        private readonly string _applicationName;
        #endregion

        #region Ctors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LoggerService" /> class.
        /// </summary>
        public LoggerService()
        {
            this._applicationName = "Movie Watcher";
            this._logger = NLogger.GetLogger("movieWatcher");
        }

        #endregion

        #region Interfaces Implementation

        /// <summary>
        ///     Gets the logger.
        /// </summary>
        /// <returns></returns>
        public Logger GetLogger()
        {
            return this._logger;
        }
        /// <summary>
        ///     Save the log message
        /// </summary>
        /// <param name="message">The log message</param>
        /// <param name="messageType">Type of logged message</param>
        /// <returns></returns>
        public void LogMessage(LogMessage message, LogLevel messageType)
        {
            if (message != null)
            {
                message.Application = this._applicationName;
                this._logger.Log(messageType, "{@LogMessage}", message);
            }
        }

        /// <summary>
        ///     Save the log message
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="messageType">Type of logged message</param>
        /// <returns></returns>
        public void LogMessage(string message, LogLevel messageType)
        {
            if (!string.IsNullOrEmpty(message))
            {
                this._logger.Log(messageType, message);
            }
        }
        #endregion
    }
}
