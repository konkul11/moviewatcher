﻿using MovieWatcher.Service.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Interfaces
{
    /// <summary>
    ///     ILoggerService interface.
    /// </summary>
    public interface ILoggerService
    {
        #region  Other Members

        /// <summary>
        ///     Gets the logger.
        /// </summary>
        /// <returns></returns>
        Logger GetLogger();
        /// <summary>
        ///     Save the log message
        /// </summary>
        /// <param name="message">The log message</param>
        /// <param name="messageType">Type of logged message</param>
        /// <returns></returns>
        void LogMessage(LogMessage message, LogLevel messageType);

        /// <summary>
        ///     Save the log message
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="messageType">Type of logged message</param>
        /// <returns></returns>
        void LogMessage(string message, LogLevel messageType);
        #endregion
    }
}
