﻿using AutoMapper;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.GenreCommands;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Profiles.GenreProfiles
{
    /// <summary>
    /// The UpdateGenreCommandProfile class implementation.
    /// This class is ssed for processing data.
    /// <seealso cref="MovieWatcher.Data.DB.Models.MWGenre"/>
    /// <seealso cref="MovieWatcher.Service.CQRS.Commands.GenreCommands.UpdateGenreCommand"/>
    /// </summary>
    public class UpdateGenreCommandProfile : Profile
    {
        /// <summary>
        /// Create map for processing data:
        ///  MWGenre to UpdateGenreCommand
        ///  UpdateGenreCommand to MWGenre
        /// </summary>
        public UpdateGenreCommandProfile()
        {
            CreateMap<MWGenre, UpdateGenreCommand>()
              .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dst => dst.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted));


            CreateMap<UpdateGenreCommand, MWGenre>()
              .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dst => dst.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted));
        }
    }
}
