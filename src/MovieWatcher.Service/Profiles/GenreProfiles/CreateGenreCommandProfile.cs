﻿using AutoMapper;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.GenreCommands;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Profiles.GenreProfiles
{
    /// <summary>
    /// The CreateGenreCommandProfile class implementation.
    /// This class is ssed for processing data.
    /// <seealso cref="MovieWatcher.Data.DB.Models.MWGenre"/>
    /// <seealso cref="MovieWatcher.Service.CQRS.Commands.GenreCommands.CreateGenreCommand"/>
    /// </summary>
    public class CreateGenreCommandProfile : Profile
    {
        /// <summary>
        /// Create map for processing data:
        ///  MWGenre to CreateGenreCommand
        ///  CreateGenreCommand to MWGenre
        /// </summary>
        public CreateGenreCommandProfile()
        {
            CreateMap<MWGenre, CreateGenreCommand>()
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<CreateGenreCommand, MWGenre>()
              .ForMember(dst => dst.Id, opt => opt.Ignore())
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dst => dst.IsDeleted, opt => opt.MapFrom(src => false));
        }
    }
}
