﻿using AutoMapper;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Profiles.GenreProfiles
{
    /// <summary>
    /// The GetGenreQueryResultProfile class implementation.
    /// This class is ssed for processing data.
    /// <seealso cref="MovieWatcher.Data.DB.Models.MWGenre"/>
    /// <seealso cref="MovieWatcher.Service.CQRS.QueryResults.GenreQueryResult.GetGenreQueryResult"/>
    /// </summary>
    public class GetGenreQueryResultProfile : Profile
    {
        /// <summary>
        /// Create map for processing data:
        ///  MWGenre to GetGenreQueryResult
        ///  GetGenreQueryResult to MWGenre
        /// </summary>
        public GetGenreQueryResultProfile()
        {
            CreateMap<MWGenre, GetGenreQueryResult>()
              .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));


            CreateMap<GetGenreQueryResult, MWGenre>()
              .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dst => dst.IsDeleted, opt => opt.Ignore());
        }
    }
}
