﻿using AutoMapper;
using MovieWatcher.Data.DB.Models;
using MovieWatcher.Service.CQRS.Commands.GenreCommands;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Profiles.GenreProfiles
{
    /// <summary>
    /// The DeleteGenreCommandProfile class implementation.
    /// This class is ssed for processing data.
    /// <seealso cref="MovieWatcher.Data.DB.Models.MWGenre"/>
    /// <seealso cref="MovieWatcher.Service.CQRS.Commands.GenreCommands.DeleteGenreCommand"/>
    /// </summary>
    public class DeleteGenreCommandProfile : Profile
    {
        /// <summary>
        /// Create map for processing data:
        ///  MWGenre to DeleteGenreCommand
        ///  DeleteGenreCommand to MWGenre
        /// </summary>
        public DeleteGenreCommandProfile()
        {
            CreateMap<MWGenre, DeleteGenreCommand>()
              .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dst => dst.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted));

            CreateMap<DeleteGenreCommand, MWGenre>()
              .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dst => dst.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted));
        }
    }
}
