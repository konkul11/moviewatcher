﻿using MovieWatcher.Service.Profiles.GenreProfiles;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Service.Profiles.ProfilesConfiguration
{
    /// <summary>
    /// The GenreProfiles class implementation.
    /// </summary>
    public static class GenreProfiles
    {
        /// <summary>
        /// Initialize genre profiles for automapper.
        /// </summary>
        /// <returns>The array of genre profiles.</returns>
        public static Type[] Initialize()
        {
            return new[]
            {
                typeof(CreateGenreCommandProfile),
                typeof(DeleteGenreCommandProfile),
                typeof(GetGenreQueryResultProfile),
                typeof(UpdateGenreCommandProfile)
            };
        }
    }
}
