﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieWatcher.Api.Config;
using MovieWatcher.Service.Interfaces;
using MovieWatcher.Service.Services;
using MovieWatcher.Data.DB;
using AutoMapper;
using MovieWatcher.Service.CQRS.Queries.UserQueries;

namespace MovieWatcher
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //// Auto Mapper Configurations
            //var mappingConfig = new MapperConfiguration(mc =>
            //{
            //    mc.ValidateInlineMaps = false;
            //    mc.CreateMissingTypeMaps = true;
            //});
            //IMapper mapper = mappingConfig.CreateMapper();
            //services.AddSingleton(mapper);
            MappingConfig.ConfigureMapper();
            services.AddDbContext<MWContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString(MWContext.ConnectionString)));



            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.ValidateInlineMaps = false;
                mc.CreateMissingTypeMaps = true;
            });
            services.AddMediatR();
            services.AddTransient<ILoggerService,LoggerService>();
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // if you have handlers/events in other assemblies
            services.AddMediatR(typeof(GetUserByLoginQuery).Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
