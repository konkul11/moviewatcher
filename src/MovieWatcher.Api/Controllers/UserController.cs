﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieWatcher.Common.Helpers;
using MovieWatcher.Service.CQRS.Commands.RegisterUserCommand;
using MovieWatcher.Service.CQRS.Models.UserModels;
using MovieWatcher.Service.CQRS.Queries.UserQueries;
using MovieWatcher.Service.Interfaces;
using NLog;

namespace MovieWatcher.Api.Controllers.UserController
{
    [Route(ControllerPaths.UserController)]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILoggerService _logger;


        public UserController(IMediator mediator, ILoggerService logger)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [Route(RouteContainer.User_GetByName)]
        [HttpGet]
        public async Task<IActionResult> GetByLogin([FromQuery] GetUserByLoginModel model)
        {
            try
            {
                _logger.LogMessage("cvcujcv",LogLevel.Info);
                GetUserByLoginQuery getUserByLoginQuery = Mapper.Map<GetUserByLoginQuery>(model);
                var result = await _mediator.Send(getUserByLoginQuery);
                if (result == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound);
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [Route(RouteContainer.User_Register)]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterUserModel model)
        {
            try
            {
                GetUserByLoginQuery getUserByLoginQuery = Mapper.Map<GetUserByLoginQuery>(model);
                var result = await _mediator.Send(getUserByLoginQuery);
                if (result == null)
                {
                    CreateUserCommand registerUserCommand = Mapper.Map<CreateUserCommand>(model);
                    var userId = await _mediator.Send(registerUserCommand);
                    //return StatusCode(StatusCodes.Status404NotFound);
                    return Ok(userId);
                }
                else
                {
                    return StatusCode(StatusCodes.Status409Conflict);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}