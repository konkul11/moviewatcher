﻿
namespace MovieWatcher.Api.Controllers
{
    #region Using
    using System;
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using MovieWatcher.Common.Enums;
    using MovieWatcher.Common.Helpers;
    using MovieWatcher.Service.CQRS.Commands.GenreCommands;
    using MovieWatcher.Service.CQRS.Queries.GenreQueries;
    using MovieWatcher.Service.Interfaces;
    using MovieWatcher.Service.Models;
    using NLog;
    #endregion

    /// <summary>
    /// The GenreController class implementation.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route(ControllerPaths.GenreController)]
    [ApiController]
    public class GenreController : ControllerBase
    {
        #region Fields
        /// <summary>
        /// The mediator.
        /// </summary>
        private readonly IMediator _mediator;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService _logger;
        #endregion

        #region Ctors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenreController" /> class.
        /// </summary>
        /// <param name="mediator">The mediator.</param>
        /// <param name="loggerService">The logger service.</param>
        /// <exception cref="ArgumentNullException">
        /// The error mediator can not be null.
        /// or
        /// The error logger service can not be null.
        /// </exception>
        public GenreController(IMediator mediator, ILoggerService loggerService)
        {
            if (mediator == null)
                throw new ArgumentNullException("The error mediator can not be null.");
            if (loggerService == null)
                throw new ArgumentNullException("The error logger service can not be null.");

            this._mediator = mediator;
            this._logger = loggerService;
        }
        #endregion

        // GET: api/v1/genres/
        /// <summary>
        /// Get all genres objects.
        /// </summary>
        /// <returns></returns>
        [Route(RouteContainer.Genre_GetAll)]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var result = await this._mediator.Send(new GetAllGenreQuery());

            if (result != null)
                return Ok(result);
            else
                return StatusCode(StatusCodes.Status404NotFound);
        }

        // GET: api/genres/5
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route(RouteContainer.Genre_GetById)]
        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            if (id > 0)
            {
                var result = await this._mediator.Send(new GetGenreByIdQuery { Id = id });
                if (result != null)
                    return Ok(result);
                else
                    return StatusCode(StatusCodes.Status404NotFound);
            }
            else
                return StatusCode(StatusCodes.Status416RequestedRangeNotSatisfiable);
        }


        // POST: api/genres
        /// <summary>
        /// Create new genre object,
        /// </summary>
        /// <param name="createCommand">The create command object.</param>
        [Route(RouteContainer.Genre_CreateGenre)]
        [HttpPost]
        public async Task<IActionResult> CreateGenre([FromBody]CreateGenreCommand createCommand)
        {
            this._logger.LogMessage("Start creating genre", LogLevel.Info);
            if (createCommand != null && !string.IsNullOrEmpty(createCommand.Name))
            {
                var result = await this._mediator.Send(createCommand);
                if (result != ActionStatus.Created)
                {
                    this._logger.LogMessage(new LogMessage()
                    {
                        Message = "The error while creating new genre",
                        ClassName = this.ToString(),
                        MethodName = "CreateGenre([FromBody]CreateGenreCommand createCommand)",
                        LogLevel = MessageType.Error,
                        Data = createCommand,
                    }, LogLevel.Error);
                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
                else
                {
                    this._logger.LogMessage("Stop creating genre - genre has been created.", LogLevel.Info);
                    return Ok(result);
                }
            }
            else
            {
                this._logger.LogMessage("The error while creating genre, data cannot be null.", LogLevel.Error);
                return StatusCode(StatusCodes.Status204NoContent);
            }
        }

        // PUT: api/genres/5
        /// <summary>
        /// Updates the genre.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="updateModel">The update command model.</param>
        [Route(RouteContainer.Genre_UpdateGenre)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateGenre(int id, [FromBody]UpdateGenreCommand updateModel)
        {
            this._logger.LogMessage("Start updating genre", LogLevel.Info);
            if (updateModel != null && !string.IsNullOrEmpty(updateModel.Name) && updateModel.Id == id)
            {
                var result = await this._mediator.Send(updateModel);
                if (result != ActionStatus.Updated)
                {
                    this._logger.LogMessage(new LogMessage()
                    {
                        Message = "The error while updating genre.",
                        ClassName = this.ToString(),
                        MethodName = "UpdateGenre(int id, [FromBody]UpdateGenreCommand upateModel)",
                        LogLevel = MessageType.Error,
                        Data = new { IdFromQuery = id, UpdateCommand = updateModel }
                    }, LogLevel.Error);
                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
                else
                {
                    this._logger.LogMessage("Stop updating genre - genre has been updated.", LogLevel.Info);
                    return Ok(result);
                }
            }
            else
            {
                this._logger.LogMessage("The error while updating genre, data cannot be null.", LogLevel.Error);
                return StatusCode(StatusCodes.Status204NoContent);
            }
        }

        // DELETE: api/genres/5
        /// <summary>
        /// Deletes the genre.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="deleteCommand">The delete command model.</param>
        [Route(RouteContainer.Genre_DeleteGenre)]
        [HttpDelete]
        public async Task<IActionResult> DeleteGenre(int id, [FromBody]DeleteGenreCommand deleteCommand)
        {
            if (deleteCommand != null && !string.IsNullOrEmpty(deleteCommand.Name) && deleteCommand.Id == id)
            {
                var result = await this._mediator.Send(deleteCommand);
                if (result != ActionStatus.Deleted)
                {
                    this._logger.LogMessage(new LogMessage()
                    {
                        Message = "The error while deleting genre.",
                        ClassName = this.ToString(),
                        MethodName = "DeleteGenre(int id, [FromBody]DeleteGenreCommand deleteCommand)",
                        LogLevel = MessageType.Error,
                        Data = new { IdFromQuery = id, DeleteCommand = deleteCommand }
                    }, LogLevel.Error);
                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
                else
                {
                    this._logger.LogMessage("Stop deleting genre - genre has been deleted.", LogLevel.Info);
                    return Ok(result);
                }
            }
            else
            {
                this._logger.LogMessage("The error while deleting genre, deete command can not be null.", LogLevel.Error);
                return StatusCode(StatusCodes.Status204NoContent);
            }
        }
    }
}
