﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Api.Config
{
    public static class MappingConfig
    {
        /// <summary>
        /// Configures the mapper.
        /// </summary>
        public static void ConfigureMapper()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.ValidateInlineMaps = false;
                cfg.CreateMissingTypeMaps = true;
            });
        }
    }
}
