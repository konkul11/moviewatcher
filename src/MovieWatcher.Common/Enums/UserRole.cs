﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MovieWatcher.Common.Enums
{
   public enum UserRole
    {
        [Description("Administrator")]
        Administrator = 1,

        [Description("User")]
        User = 2,

        [Description("SuperAdministrator")]
        SuperAdministrator = 3,

        [Description("Editor")]
        Editor = 4
    }
}
