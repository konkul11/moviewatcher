﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Common.Enums
{
    /// <summary>
    /// The ActionStatus enum implementation.
    /// This class represents the action status in handler.
    /// </summary>
    public enum ActionStatus
    {
        /// <summary>
        /// The ok status.
        /// </summary>
        OK,
        /// <summary>
        /// The created status, use than when data has been created.
        /// </summary>
        Created,
        /// <summary>
        /// The updated status, use than when data has been updated.
        /// </summary>
        Updated,
        /// <summary>
        /// The deleted status, use than when data has been deleted.
        /// </summary>
        Deleted,
        /// <summary>
        /// The data has been not created - error
        /// </summary>
        NotCreated,
        /// <summary>
        /// The data has been not updated - error
        /// </summary>
        NotModified,
        /// <summary>
        /// The data has been not deleted - error
        /// </summary>
        NotDeleted,
        /// <summary>
        /// The data was not found.
        /// </summary>
        NotFound,
        /// <summary>
        /// The internal server error.
        /// </summary>
        InternalServerError,
        /// <summary>
        /// The data not created because already exists. 
        /// </summary>
        DataAlreadyExists,
        /// <summary>
        /// The data is using in another table - cannot be deleted.
        /// </summary>
        DataIsUsing
    }
}
