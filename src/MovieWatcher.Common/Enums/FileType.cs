﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MovieWatcher.Common.Enums
{
    public enum FileType
    {
        [Description("Movie")]
        Movie = 1,

        [Description("Image")]
        Image = 2,

        [Description("Subtitles")]
        Subtitles = 3
    }
}
