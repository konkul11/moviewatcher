﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MovieWatcher.Common.Enums
{
    /// <summary>
    /// The MovieType enum implementation.
    /// This class representation the type of movie, for example
    ///  -> Movie, Series, Story/Tale, Anime 
    /// </summary>
    public enum MovieType
    {
        /// <summary>
        /// The movie.
        /// </summary>
        [Description("Movie")]
        Movie,
        /// <summary>
        /// The series
        /// </summary>
        [Description("Series")]
        Series,
        /// <summary>
        /// The tale.
        /// </summary>
        [Description("Tale")]
        Tale,
        /// <summary>
        /// The anime.
        /// </summary>
        [Description("Anime")]
        Anime
    }
}
