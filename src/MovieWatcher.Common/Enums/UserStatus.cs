﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MovieWatcher.Common.Enums
{
   public enum UserStatus
    {
        [Description("Unconfirmed")]
        Unconfirmed = 1,

        [Description("Active")]
        Active = 2,

        [Description("Inactive")]
        Inactive = 3

    }
}
