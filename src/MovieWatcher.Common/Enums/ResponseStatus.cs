﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Common.Enums
{
    /// <summary>
    ///   The response status enum implementation
    /// </summary>
    public enum ResponseStatus
    {
        /// <summary>
        ///  The correct response
        /// </summary>
        OK = 200,
        /// <summary>
        ///   Successfully logged in
        /// </summary>
        OKSuccessfullyLoggedIn = 201,
        /// <summary>
        ///   The account was successfully created.
        /// </summary>
        OKCreatedAccount = 202,
        /// <summary>
        /// The account has been successfully updated.
        /// </summary>
        OKUpdatedAccount = 203,
        /// <summary>
        /// The The account has been successfully deleted.
        /// </summary>
        OKDeletedAccount = 204,
        



        /// <summary>
        ///  The data not found.
        /// </summary>
        NotFound = 404,



        /// <summary>
        ///     The Internal Server Error
        /// </summary>
        InternalServerError = 500,
        /// <summary>
        ///    Error incorrect username or password
        /// </summary>
        ErrorLoginIncorrectLoginOrPassword = 501,
        /// <summary>
        ///   The error while creating new account.
        /// </summary>
        ErrorCreatedAccount = 502,
        /// <summary>
        /// The error while updating account.
        /// </summary>
        ErrorUpdatedAccount = 503,
        /// <summary>
        /// The error while deleting account.
        /// </summary>
        ErrorDeletedAccount = 504,




       
        /// <summary>
        /// The unknown error.
        /// </summary>
        Unknown = 999
    }

}
