﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MovieWatcher.Common.Helpers
{
    /// <summary>
    /// NLogger class implementation.
    /// Static class to log information about events in application.
    /// </summary>
    public static class NLogger
    {

        /// <summary>
        /// Get logger to log info.
        /// </summary>
        /// <param name="type">The type of calling class.</param>
        /// <returns>
        /// NLog logger
        /// </returns>
        public static Logger GetLogger(Type type = null)
        {
            if (type == null)
            {
                var frame = new StackFrame(skipFrames: 1);
                var method = frame.GetMethod();
                type = method.DeclaringType;
            }
            if (type != null)
            {
                return LogManager.GetLogger(name: type.FullName);
            }
            return null;
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        /// <param name="loggername">The loggername.</param>
        /// <returns></returns>
        public static Logger GetLogger(string loggername)
        {
            return LogManager.GetLogger(name: loggername);
        }
    }
}
