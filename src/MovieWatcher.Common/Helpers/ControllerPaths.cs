﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Common.Helpers
{
    /// <summary>
    /// Controller names class implementation.
    /// Holds all controller paths used in app.
    /// </summary>
    public static class ControllerPaths
    {
        /// <summary>
        /// The API prefix.
        /// </summary>
        private const string Api_Prefix = "/api";
        /// <summary>
        /// The API version.
        /// </summary>
        private const string Api_Version = "/v1";
        /// <summary>
        /// The API full path.
        /// </summary>
        private const string Api_Path = Api_Prefix + Api_Version;

        /// <summary>
        /// The path to the User controller
        /// </summary>
        public const string UserController = Api_Path + "/users";


        /// <summary>
        /// The path to the genre controller.
        /// </summary>
        public const string GenreController = Api_Path + "/genres";
    }
}
