﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Common.Helpers
{
    /// <summary>
    /// Route container class implementation.
    /// Holds all route used in app.
    /// </summary>
    public static class RouteContainer
    {
        ///*********************************
        /// UserController Action
        ///*********************************
        /// <summary>
        /// The route is used to download the user by login / name.
        /// </summary>
        public const string User_GetByName = "GetByLogin";

        public const string User_Register = "Register";


        ///*********************************
        /// GenreController Actions
        ///*********************************
        /// <summary>
        /// The genre get all genres.
        /// </summary>
        public const string Genre_GetAll = "";
        /// <summary>
        /// The genre get by identifier.
        /// </summary>
        public const string Genre_GetById = "{id}";

        /// <summary>
        /// The genre create genre
        /// </summary>
        public const string Genre_CreateGenre = "";

        /// <summary>
        /// The genre update genre
        /// </summary>
        public const string Genre_UpdateGenre = "{id}";

        /// <summary>
        /// The genre delete genre
        /// </summary>
        public const string Genre_DeleteGenre = "{id}";
    }
}
