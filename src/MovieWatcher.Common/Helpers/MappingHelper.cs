﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Common.Helpers
{
    public static class MappingHelper
    {
        /// <summary>
        /// Configures the mapper.
        /// </summary>
        public static void ConfigureMapper()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.ValidateInlineMaps = false;
                cfg.CreateMissingTypeMaps = true;
            });
        }
    }
}
