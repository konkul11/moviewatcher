﻿using Microsoft.EntityFrameworkCore;
using MovieWatcher.Data.DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB
{
    public class MWContext : DbContext
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        public static string ConnectionString = @"Server=.\sqlexpress;Database=MWDatabase;Trusted_Connection=True;";

        public DbSet<MWActor> Actors { get; set; }

        public DbSet<MWActorFile> ActorsFiles { get; set; }

        public DbSet<MWComment> Comments { get; set; }

        public DbSet<MWCountry> Countries { get; set; }

        public DbSet<MWFavourite> Favourites { get; set; }

        public DbSet<MWGenre> Genres { get; set; }

        public DbSet<MWMovie> Movies { get; set; }

        public DbSet<MWMovieActor> MovieActors { get; set; }

        public DbSet<MWMovieCountry> MovieCountries { get; set; }

        public DbSet<MWMovieGenre> MovieGenres { get; set; }

        public DbSet<MWMovieFile> MovieFiles { get; set; }

        public DbSet<MWMovieRate> MovieRates { get; set; }

        public DbSet<MWMovieSeason> MovieSeasons { get; set; }

        public DbSet<MWMovieSeasonEpisode> MovieSeasonEpisodes { get; set; }

        public DbSet<MWMovieWatch> MovieWatches { get; set; }

        public DbSet<MWUser> Users { get; set; }

        public DbSet<MWUserToken> UserTokens { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
    }
}
