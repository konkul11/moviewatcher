using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    public class MWActor
    {
        /// <summary>
        /// The actor id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Actor first name
        /// </summary>
        [StringLength(50)]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Actor last name
        /// </summary>
        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Actor birthday date. If date is unkown then it is equal null
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// Description about actor
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Forgein key to file table.  It is used to obtain a profile photo of an actor
        /// </summary>
        public long? MainPictureMWFileId { get; set; }

        /// <summary>
        /// The actor images files
        /// </summary>
        public ICollection<MWActorFile> ImageFiles { get; set; }
    }
}