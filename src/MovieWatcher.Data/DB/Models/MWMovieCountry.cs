using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieCountry class implementation.
    /// </summary>
    public class MWMovieCountry
    {
        /// <summary>
        /// The movie country id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// The forgein key to country table
        /// </summary>
        public long MWCountryId { get; set; }

        /// <summary>
        /// Indicates whether the movieCountry has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }

        /// <summary>
        /// Virual field to refer to the country table
        /// </summary>
        public virtual MWCountry MWCountry { get; set; }
    }
}
