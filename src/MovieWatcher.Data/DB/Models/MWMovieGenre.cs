﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieGenre class implementation.
    /// </summary>
    public class MWMovieGenre
    {
        /// <summary>
        /// The movie genre id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The forgein key to genre table
        /// </summary>
        public long MWGenreId { get; set; }

        /// <summary>
        /// The forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Indicates whether the movieGenre has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Virual field to refer to the genre table
        /// </summary>
        public virtual MWGenre MWGenre { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }
    }
}
