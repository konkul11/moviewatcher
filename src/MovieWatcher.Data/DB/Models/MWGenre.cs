using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWGenre class implementation.
    /// </summary>
    public class MWGenre
    {
        /// <summary>
        /// The genre id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Genre name
        /// </summary>
        [StringLength(55)]
        public string Name { get; set; }

        /// <summary>
        /// Indicates whether the genre has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
