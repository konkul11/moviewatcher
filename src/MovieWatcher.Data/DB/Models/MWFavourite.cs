using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWFavourite class implementation.
    /// </summary>
    public class MWFavourite
    {
        /// <summary>
        /// The favourite movie id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The forgein key to movie table
        /// </summary>
        public long  MWMovieId { get; set; }

        /// <summary>
        /// The forgein key to user table
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Indicates whether the favourite movie has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }

        /// <summary>
        /// Virual field to refer to the user table
        /// </summary>
        public virtual MWUser MWUser{ get; set; }
    }
}
