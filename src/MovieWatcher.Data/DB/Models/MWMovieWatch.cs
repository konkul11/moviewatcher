using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieWatch class implementation.
    /// </summary>
    public class MWMovieWatch
    {
        /// <summary>
        /// The movie watched id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Forgein key to user table
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Whether the film was watched until the end
        /// </summary>
        public bool IsFullyWatched { get; set; }

        /// <summary>
        /// The start date of watching the movie
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Time of watching the movie
        /// </summary>
        public TimeSpan WatchingTime { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }

        /// <summary>
        /// Virual field to refer to the user table
        /// </summary>
        public virtual MWUser MWUser { get; set; }
    }
}
