using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWFileBase class implementation.
    /// </summary>
    public abstract class MWFileBase
    {
        /// <summary>
        ///The file id 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Original file name
        /// </summary>
        [StringLength(255)]
        [Required]
        public string OriginalFileName { get; set; }

        /// <summary>
        /// Server file name
        /// </summary>
        [StringLength(255)]
        [Required]
        public string ServerFileName { get; set; }

        /// <summary>
        /// File extension 
        /// </summary>
        [StringLength(10)]
        [Required]
        public string Extension { get; set; }

        /// <summary>
        /// The guid
        /// </summary>
        public Guid UniqueGuid { get; set; }

        /// <summary>
        /// File size in bytes
        /// </summary>
        public long SizeInBytes { get; set; }

        /// <summary>
        /// The date the file was added
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Specifies the file type
        /// </summary>
        public FileType Type { get; set; }

        /// <summary>
        /// Indicates whether the file has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
