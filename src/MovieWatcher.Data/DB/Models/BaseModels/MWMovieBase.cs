﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models.BaseModels
{
    /// <summary>
    /// The movie base implementation
    /// </summary>
    public abstract class MWMovieBase
    {
        /// <summary>
        /// Gets or sets movie id.
        /// </summary>
        /// <value>
        /// The movie id.
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the duration time.
        /// </summary>
        /// <value>
        /// The duration time.
        /// </value>
        public TimeSpan DurationTime { get; set; }
        /// <summary>
        /// Gets or sets the movie title.
        /// </summary>
        /// <value>
        /// The movie title.
        /// </value>
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets polish movie title.
        /// </summary>
        /// <value>
        /// The polish movie title.
        /// </value>
        [Required]
        [MaxLength(255)]
        public string Title_PL { get; set; }
        /// <summary>
        /// Gets or sets the movie description.
        /// </summary>
        /// <value>
        /// The movie description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the release date
        /// </summary>
        /// <value>
        /// The release date.
        /// </value>
        public DateTime ReleaseDate { get; set; }
        /// <summary>
        /// Gets or sets the release date in poland.
        /// </summary>
        /// <value>
        /// The release date in poland.
        /// </value>
        public DateTime ReleaseDate_PL { get; set; }
        /// <summary>
        /// Gets or sets the user who has updated data.
        /// </summary>
        /// <value>
        /// The user who has updated data.
        /// </value>
        public long InsertMWUserId { get; set; }
        /// <summary>
        /// Gets or sets the insert date.
        /// </summary>
        /// <value>
        /// The insert date.
        /// </value>
        public DateTime InsertDate { get; set; }
        /// <summary>
        /// Gets or sets the update date.
        /// </summary>
        /// <value>
        /// The update date.
        /// </value>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// Gets or sets the sound type id.
        /// </summary>
        /// <value>
        /// The sound type id.
        /// </value>
        public long MWSoundTypeId { get; set; }
        /// <summary>
        /// Gets or sets has subtitles.
        /// When the movie has an original soundtrack, probably has a subtitle file.
        /// </summary>
        /// <value>
        /// The has subtitles.
        /// </value>
        public bool HasSubtites { get; set; }

        //ToDo:
        //The PAth to the subtitles 
        //I do know how to write / add this :( 
        // Can you help me ?
        //Because i think we have to change this model in the DB, but maybe you have some ideas :)

        /// <summary>
        /// Gets or sets isAccepted.
        /// If the movie is not accepted then we can not display it!!! 
        /// </summary>
        /// <value>
        /// The isAccpeted.
        /// </value>
        public bool IsAccepted { get; set; }
        /// <summary>
        /// Gets or sets the deleted movie.
        /// </summary>
        /// <value>
        /// The deleted movie.
        /// </value>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        public virtual MWUser MWUser { get; set; }
        /// <summary>
        /// Gets or sets the sound type.
        /// </summary>
        /// <value>
        /// The sound type.
        /// </value>
        public virtual MWSoundType MWSoundType { get; set; }
    }
}
