using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWComment class implementation.
    /// </summary>
    public class MWComment
    {
        /// <summary>
        /// The comment id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Comment text
        /// </summary>
        [StringLength(250)]
        [Required]
        public string Text { get; set; }    

        /// <summary>
        /// Forgein key to user table
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Date when comment was added
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Comment upadte date 
        /// </summary>
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Indicates whether the comment has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Virual field to refer to the user table
        /// </summary>
        public virtual MWUser MWUser { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }
    }
}