using MovieWatcher.Common.Enums;
using MovieWatcher.Data.DB.Models.BaseModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovie class implementation.
    /// </summary>
    public class MWMovie : MWMovieBase
    {
        /// <summary>
        /// Get or sets the movie type
        /// </summary>
        /// <value>
        /// The movie type.
        /// </value>
        public MovieType MovieType { get; set; }

        /// <summary>
        /// The movie files
        /// </summary>
        public ICollection<MWMovieFile> MovieFiles { get; set; }
    }
}
