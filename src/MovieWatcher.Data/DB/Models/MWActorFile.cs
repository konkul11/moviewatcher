﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWActorFile class implementation.
    /// </summary>
    public class MWActorFile : MWFileBase
    {
        /// <summary>
        /// The forgein key to Actor table
        /// </summary>
        public long MWActorId { get; set; }
        /// <summary>
        /// Gets or sets the main poster for actor.
        /// </summary>
        /// <value>
        ///  The main poster for actor.
        /// </value>
        public bool IsMainPoster { get; set; }
        /// <summary>
        /// Gets or sets mini poster for actor.
        /// </summary>
        /// <value>
        ///  The mini poster.
        /// </value>
        public bool IsMiniPoster { get; set; }

        /// <summary>
        /// Virual field to refer to the actor table
        /// </summary>
        public virtual MWActor MWActor { get; set; }
    }
}
