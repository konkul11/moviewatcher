﻿using MovieWatcher.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWUserToken class implementation.
    /// </summary>
    public class MWUserToken
    {
        /// <summary>
        /// The user token id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The token
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// The insert date
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Token expiration date 
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Forgein key to user table
        /// </summary>
        public long MWUserId { get; set; }

        /// <summary>
        /// Date when token was used
        /// </summary>
        public DateTime? UsedDate { get; set; }

        /// <summary>
        /// Token type
        /// </summary>
        public TokenType Type { get; set; }

        /// <summary>
        /// Virual field to refer to the user table
        /// </summary>
        public virtual MWUser MWUser { get; set; }
    }
}
