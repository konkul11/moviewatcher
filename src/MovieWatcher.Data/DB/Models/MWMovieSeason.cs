﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieSeason class implementation.
    /// </summary>
    public class MWMovieSeason
    {
        /// <summary>
        /// The movie season id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Season number
        /// </summary>
        public byte SeasonNumber { get; set; }

        /// <summary>
        /// Forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Indicates whether the movieSeason has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }
    }
}
