using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWCountry class implementation.
    /// </summary>
    public class MWCountry
    {
        /// <summary>
        /// The country id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The country name
        /// </summary>
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Indicates whether the country has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
