using System.ComponentModel.DataAnnotations;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieActor class implementation.
    /// </summary>
    public class MWMovieActor
    {
        /// <summary>
        /// The  movie actor id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Forgein key to actor table
        /// </summary>
        public long MWActorId { get; set; }

        /// <summary>
        /// Indicates whether the movieActor has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// The actor character name
        /// </summary>
        [Required]
        [StringLength(100)]
        public string CharacterName { get; set; }

        /// <summary>
        /// Virual field to refer to the actor table
        /// </summary>
        public virtual MWActor MWActor { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }
    }
}
