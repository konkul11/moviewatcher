﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieSeasonEpisode class implementation.
    /// </summary>
    public class MWMovieSeasonEpisode
    {
        /// <summary>
        /// Gets or sets the episode id.
        /// </summary>
        /// <value>
        /// The episode id
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets the episode number.
        /// </summary>
        /// <value>
        /// The episode number
        /// </value>
        public short EpisodeNumber { get; set; }
        /// <summary>
        /// Gets or sets the movie season id.
        /// </summary>
        /// <value>
        /// The movie season id
        /// </value>
        public long MWMovieSeasonId { get; set; }
        /// <summary>
        /// Gets or sets the episode is deleted?
        /// </summary>
        /// <value>
        /// Is deleted
        /// </value>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Gets or sets the season.
        /// </summary>
        /// <value>
        /// The movie season.
        /// </value>
        public virtual MWMovieSeason MWMovieSeason { get; set; }
    }
}
