﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The Sound type class implementation.
    /// This class represents the type of soundtrack in the movie:
    /// - original ( + Subtitles)
    /// - translated
    ///   -> lector
    ///   -> dubbing 
    /// </summary>
    public class MWSoundType
    {
        /// <summary>
        /// Gets or sets sound type id.
        /// </summary>
        /// <value>
        /// The sound type id.
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets sound type name.
        /// </summary>
        /// <value>
        /// The sound type name.
        /// </value>
        [StringLength(55)]
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the sound type is deleted.
        /// </summary>
        /// <value>
        /// The sound type is deleted.
        /// </value>
        public bool IsDeleted { get; set; }
    }
}
