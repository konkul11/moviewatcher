﻿using MovieWatcher.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWUser class implementation.
    /// </summary>
    public class MWUser
    {
        /// <summary>
        /// The user id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        [Column(TypeName = "VARCHAR(50)")]
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// User email
        /// </summary>
        [Column(TypeName = "VARCHAR(50)")]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// The password
        /// </summary>
        [Column(TypeName = "VARCHAR(128)")]
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// The last login date. Is null until the user first logs on
        /// </summary>
        public DateTime? LastLoginDate { get; set; }

        /// <summary>
        /// Date when user registred 
        /// </summary>
        public DateTime RegistrationDate { get; set; }

        /// <summary>
        ///  Date when user confirmed rejstration
        /// </summary>
        public DateTime? ConfirmationDate { get; set; }


        /// <summary>
        /// The salt to code password
        /// </summary>
        public Guid Salt { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        [StringLength(30)]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// The user last name
        /// </summary>
        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// The role
        /// </summary>
        public UserRole Role { get; set; }

        /// <summary>
        /// Indicates whether the user has been deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// User status
        /// </summary>
        public UserStatus Status { get; set; }
    }
}
