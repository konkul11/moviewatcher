using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieRate class implementation.
    /// </summary>
    public class MWMovieRate
    {
        /// <summary>
        /// Movie rate id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Forgeun key to user table
        /// </summary>
        public long MWUserId { get; set; }    

        /// <summary>
        /// Forgein key to movie table
        /// </summary>
        public long MWMovieId { get; set; }   

        /// <summary>
        /// Date added movie rate
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Rating from 1 to 10 
        /// </summary>
        public byte Rate { get; set; }

        /// <summary>
        /// Virual field to refer to the user table
        /// </summary>
        public virtual MWUser MWUser { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }
    }
}