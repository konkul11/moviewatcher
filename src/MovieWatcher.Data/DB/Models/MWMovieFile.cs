﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieWatcher.Data.DB.Models
{
    /// <summary>
    /// The MWMovieFile class implementation.
    /// </summary>
    public class MWMovieFile : MWFileBase
    {
        /// <summary>
        /// The forgein key to Movie table
        /// </summary>
        public long MWMovieId { get; set; }

        /// <summary>
        /// Virual field to refer to the movie table
        /// </summary>
        public virtual MWMovie MWMovie { get; set; }
    }
}